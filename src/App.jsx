import './App.css';
import React from 'react';
import Footer from './component/Footer';
import Header from './component/Header';
import Main from './component/Main';

function App() {
  return (
    <div className="app">
      <Header />
      <Main />
      <Footer />
    </div>
  );
}

export default App;
