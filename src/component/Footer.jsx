import React from 'react';
import PropTypes from 'prop-types';
const Footer = (props) => {
  return (
    <>
      <h1 style={{ paddingBottom: '50px', marginTop: '40px' }}>{props.title}</h1>
    </>
  );
};

Footer.defaultProps = {
  title: 'Footer title',
};

Footer.propTypes = {
  title: PropTypes.string.isRequired,
};

export default Footer;
