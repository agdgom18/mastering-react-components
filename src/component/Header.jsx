import React from 'react';
import PropTypes from 'prop-types';
const Header = (props) => {
  return (
    <>
      <h1 style={{ color: 'green', marginBottom: '30px' }}>{props.title}</h1>
    </>
  );
};

Header.defaultProps = {
  title: 'Header title',
};

Header.propTypes = {
  title: PropTypes.string.isRequired,
};

export default Header;
