import React from 'react';
import PhotoItems from '../PhotoItems';

const Main = ({ title }) => {
  const [items, setItems] = React.useState([]);
  fetch('https://jsonplaceholder.typicode.com/photos').then((res) => res.json().then((data) => setItems(data.slice(0, 10))));
  return (
    <>
      <h2 style={{ color: 'green' }}>{title}</h2>

      <PhotoItems>
        <div className="wrapper">
          {items.map((el) => {
            return (
              <div className="card" key={el.id}>
                <h4 className="fd">{el.title}</h4>
                <img src={el.url} alt="logo" />
              </div>
            );
          })}
        </div>
      </PhotoItems>
    </>
  );
};

export default Main;
